usage: main.py [-h] [-s S] [-o O] source replica

Folder synchronization software.

positional arguments:
  source      Source directory
  replica     Replica directory

options:
  -h, --help  show this help message and exit
  -s S        Data synchronization frequency in seconds (default: 10)
  -o O        Data synchronization frequency in seconds (default: data.log)
Example:
  python main.py source replica -s 10 -o data.log
  python main.py source replica
