import os
import shutil
import hashlib
import time
import argparse
import logging
from sys import platform

parser = argparse.ArgumentParser(description='Folder synchronization software.')
parser.add_argument('source', type=str, help='Source directory')
parser.add_argument('replica', type=str, help='Replica directory')
parser.add_argument('-s', type=int, default=10, help='Data synchronization frequency in seconds (default: 10)')
parser.add_argument('-o', type=str, default='data.log', help='Data synchronization frequency in seconds (default: data.log)')
args = parser.parse_args()

src = args.source
rep = args.replica
timeout = args.s
logfile = args.o

if platform == "win32":
    sep = '\\'
else:
    sep = '/'

logging.basicConfig(filename=logfile, level=logging.INFO)

def unfakeroot(fakeroot: str, root: str) -> str:
    return f'{root}{sep}{sep.join(fakeroot.split(sep)[1:])}'

def fakeroot(root: str) -> str:
    return f'fakeroot{sep}{sep.join(root.split(sep)[1:])}'

def fakerootlist(flist: list) -> list:
    flst = []
    for lst in flist:
        flst.append(f'fakeroot{sep}{sep.join(lst.split(sep)[1:])}')
    return flst

def md5(fname: str) -> str:
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def md5_and_file_to_dict(filelist: list) -> dict:
    dct = {}
    for file in filelist:
        dct[fakeroot(file)] = md5(file)
    return dct

def get_path(fldr: str) -> tuple:
    filelist = []
    folderlist = []
    for path, folders, files in os.walk(fldr):
        for file in files:
            filelist.append(f'{path}{sep}{file}')
        for folder in folders:
            folderlist.append(f'{path}{sep}{folder}')
    return((filelist,folderlist))

def dirs_control(rep: str, src: str) -> bool:
    rep_folders = fakerootlist(get_path(rep)[1])
    src_folders = fakerootlist(get_path(src)[1])
    sresult = list(set(src_folders) - set(rep_folders))
    rresult = list(set(rep_folders) - set(src_folders))
    sresult.sort()
    #rresult.sort()
    if len(sresult) != 0:
        for forder in sresult:
            folder = unfakeroot(forder,rep)
            print(f'Missing folder: {folder}')
            os.mkdir(folder)
            print(f'Folder created: {folder}')
    if len(rresult) != 0:
        for forder in rresult:
            folder = unfakeroot(forder,rep)
            print(f'Extra folder found: {folder}')
            if os.path.exists(folder):
                shutil.rmtree(folder)
            print(f'Folder deleted: {folder}')
    return True

def file_control(filesdict_src: list, filesdict_rep: list, rep: str, src: str) -> bool:
    if filesdict_src != filesdict_rep:
        keys = []
        rep_key = filesdict_src.keys() - filesdict_rep.keys()
        src_key = filesdict_rep.keys() - filesdict_src.keys()
        if rep_key:
            for key in rep_key:
                rfile = unfakeroot(key,rep)
                sfile = unfakeroot(key,src)
                logging.info(f'No file: {rfile}')
                print(f'No file: {rfile}')
                shutil.copy2(sfile, rfile)
                logging.info(f'Copy file {sfile} in {rfile}')
                print(f'Copy file {sfile} in {rfile}')
        if src_key:
            for key in src_key:
                rfile = unfakeroot(key,rep)
                logging.info(f'Extra file found: {rfile}')
                print(f'Extra file found: {rfile}')
                if os.path.exists(rfile):
                    os.remove(rfile)
                logging.info(f'File deleted: {rfile}')
                print(f'File deleted: {rfile}')
        filesdict_src = md5_and_file_to_dict(get_path(src)[0])
        filesdict_rep = md5_and_file_to_dict(get_path(rep)[0]) 

        for key, value in filesdict_src.items():
            if filesdict_rep.get(key) != value:
                unfs = unfakeroot(key,src)
                unfr = unfakeroot(key,rep)
                logging.info(f'File mismatch: {unfs} and {unfr}\nMD5-{src}: {value}\nMD5-{rep}: {filesdict_rep.get(key)}')
                print(f'File mismatch: {unfs} and {unfr}\nMD5-{src}: {value}\nMD5-{rep}: {filesdict_rep.get(key)}')
                shutil.copy2(unfs,unfr)
                logging.info(f'Copy file {unfs} in {unfr}')
                print(f'Copy file {unfs} in {unfr}')
    return True


while True:
    filesdict_src = md5_and_file_to_dict(get_path(src)[0])
    filesdict_rep = md5_and_file_to_dict(get_path(rep)[0])
    dirs_control(rep,src)
    file_control(filesdict_src,filesdict_rep,rep,src)
    time.sleep(timeout)